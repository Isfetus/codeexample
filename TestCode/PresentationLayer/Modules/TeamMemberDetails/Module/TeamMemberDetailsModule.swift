//
//  TeamMemberDetailsModule.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

import UIKit

final class TeamMemberDetailsModule: ModuleProtocol {
    let presenter: TeamMemberDetailsPresenter
    let viewController: UIViewController

    // MARK: - TeamMemberDetailsModuleProtocol

    var input: TeamMemberDetailsModuleInput?
    var output: TeamMemberDetailsModuleOutput? {
        didSet {
            presenter.moduleOutput = output
        }
    }

    init(controller: TeamMemberDetailsViewController,
         presenter: TeamMemberDetailsPresenter) {
        viewController = controller
        self.presenter = presenter
    }
}
