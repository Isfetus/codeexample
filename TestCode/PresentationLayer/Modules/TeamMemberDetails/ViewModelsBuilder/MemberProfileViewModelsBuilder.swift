//
//  MemberProfileViewModelsBuilder.swift
//  TestCode
//
//  Created by Andrey Belkin on 23/05/2020.
//

import UIKit

final class MemberProfileViewModelsBuilder: MemberProfileViewModelsBuilderProtocol {
    // MARK: - MemberProfileViewModelsBuilderProtocol

    func viewModel(for profile: TeamMemberDTO) -> [TitledTextViewModelProtocol] {
        var personalInfo: [TitledTextViewModelProtocol] = []

        let displayName: String
        if let fullName = profile.fullName,
            !fullName.isEmpty {
            displayName = fullName
        } else {
            let fullNameArray = [profile.firstName,
                                 profile.lastName].compactMap { $0 }
            displayName = fullNameArray.joined(separator: " ")
        }

        let initials = displayName.displayNameInitials()
        let initialsModel = TitledTextViewModel(title: displayName,
                                                text: initials)
        personalInfo.append(initialsModel)

        let fullNameArray = [profile.firstName,
                             profile.middleName,
                             profile.lastName].compactMap { $0 }
        let fullName = fullNameArray.joined(separator: " ")

        let fullNameModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.name,
                                                text: fullName)
        personalInfo.append(fullNameModel)

        if let type = profile.type,
            !type.isEmpty {
            let typeModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.type,
                                                text: type)
            personalInfo.append(typeModel)
        }

        if let designation = profile.designation,
            !designation.isEmpty {
            let designationModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.designation,
                                                       text: designation)
            personalInfo.append(designationModel)
        }

        if let phone = profile.phone,
            !phone.isEmpty {
            let phoneModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.phone,
                                                 text: phone)
            personalInfo.append(phoneModel)
        }

        if let email = profile.email,
            !email.isEmpty {
            let emailModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.email,
                                                 text: email)
            personalInfo.append(emailModel)
        }

        if let address = profile.address,
            !address.isEmpty {
            let addressModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.address,
                                                   text: address)
            personalInfo.append(addressModel)
        }

        if let location = profile.location,
            !location.isEmpty {
            let locationModel = TitledTextViewModel(title: L10n.Profile.Table.Cell.location,
                                                    text: location)
            personalInfo.append(locationModel)
        }

        return personalInfo
    }
}
