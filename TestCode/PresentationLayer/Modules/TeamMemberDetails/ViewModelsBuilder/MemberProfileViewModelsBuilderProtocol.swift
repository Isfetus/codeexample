//
//  TeamMemberProfileViewModelsBuilderProtocol.swift
//  TestCode
//
//  Created by Andrey Belkin on 23/05/2020.
//

/**
 Builder for creating member personal data view models.
 */
protocol MemberProfileViewModelsBuilderProtocol {

    /// Method returns array of user info text view models.
    ///
    /// - Parameter profile: Team member data.
    /// - Returns: Titled texts view models`.
    func viewModel(for profile: TeamMemberDTO) -> [TitledTextViewModelProtocol]
}
