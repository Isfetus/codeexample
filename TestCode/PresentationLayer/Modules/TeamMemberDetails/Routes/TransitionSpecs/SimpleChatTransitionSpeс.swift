//
//  SimpleChatTransitionSpec.swift
//  TestCode
//
//  Created by Andrey Belkin on 15.12.2020.
//

import PromiseKit

struct SimpleChatTransitionSpec: Presenting {
    typealias TargetModuleType = ChatModule

    let maker: Makeable
    weak var sourceInput: TeamMemberDetailsModuleInput?
    weak var sourceController: UIViewController!
    private var targetModule: TargetModuleType!

    init(maker: Makeable) {
        self.maker = maker
    }

    mutating func configure(with teamMemberId: String?) {
        guard
            let teamMemberId = teamMemberId,
            sourceController != nil else {
            fatalError()
        }

        targetModule = maker.makeTargetModule()

        guard targetModule != nil else {
            fatalError()
        }

        targetModule.output = sourceInput
        targetModule.input?.configure(teamMemberId: teamMemberId)
        targetModule.viewController.hidesBottomBarWhenPushed = true
    }

    func configuredSourceController() -> UIViewController {
        sourceController
    }

    func configuredTargetController() -> UIViewController {
        let navigationController = NavigationController(rootViewController: targetModule.viewController)
        navigationController.modalPresentationStyle = .fullScreen
        return navigationController
    }
}

extension Route where TransitionType == SimpleChatTransitionSpec {
    @discardableResult
    func go(teamMemberId: String) -> Promise<Void> {
        transition.configure(with: teamMemberId)

        return transition.go()
    }
}
