//
//  NewMessageRoute.swift
//  TestCode
//
//  Created by Andrey Belkin on 22/05/2020.
//

typealias TeamMemberDetailsNewMessageRoute = ModuleRoute<TeamMemberDetailsModule,
    ChatModule,
    SimpleChatTransitionSpec>
