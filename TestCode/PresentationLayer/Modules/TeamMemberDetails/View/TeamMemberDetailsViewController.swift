//
//  TeamMemberDetailsViewController.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

import RxSwift
import UIKit

class TeamMemberDetailsViewController: UIViewController {
    var output: TeamMemberDetailsViewOutput!
    var viewModel: ProfileViewModelProtocol? {
        didSet {
            bindViewModel()
        }
    }

    private let dataSource = ProfileCollectionViewDataSource()
    private let collectionViewLayout = ProfileLayout()
    private let disposeBag = DisposeBag()

    // MARK: - Docks

    private lazy var dock: DockProtocol = ViewControllerDock(viewController: self)
        .collectionViewDock
        .pullToRefreshDock
        .loadingIndicatorDock

    // MARK: - View Hierarchy

    private lazy var collectionView: UICollectionView = dock.collectionViewDock.collectionView
    private lazy var refreshControl: UIRefreshControl = dock.pullToRefreshDock.refreshControl
    private lazy var activityIndicator: UIActivityIndicatorView = dock.loadingIndicatorDock.activityIndicator

    private var barTintColor: UIColor?

    // MARK: - LifeCycle

    init() {
        super.init(nibName: nil,
                   bundle: nil)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        output.viewDidTriggerReadyEvent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        barTintColor = navigationController?.navigationBar.barTintColor
        navigationController?.navigationBar.barTintColor = Constants.navBarTintColor
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = barTintColor
    }
}

extension TeamMemberDetailsViewController: TeamMemberDetailsViewInput {
    // MARK: - TeamMemberDetailsViewInput

    func setupInitialState() {
        setupViewElements()
        setupConstraints()
    }

    func setViewModel(_ model: ProfileViewModelProtocol) {
        viewModel = model
    }
}

private extension TeamMemberDetailsViewController {
    // MARK: - Constants

    enum Constants {
        static let refreshControlHidden: CGFloat = 0
        static let refreshControlVisible: CGFloat = 1
        // Texts
        static let newMessageTitle: String = L10n.TeamMemberDetails.Button.newMessage
        // Colors
        static let navBarTintColor = UIColor(named: .cityLights)
    }
}

private extension TeamMemberDetailsViewController {
    // MARK: - Private

    func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }

        dataSource.profileHeaderViewModel = viewModel.headerViewModel

        viewModel.personalInfo.drive(onNext: { [unowned self] viewModels in
            self.dataSource.data = viewModels
            self.collectionView.reloadData()
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.refreshControl.endRefreshing()
        }).disposed(by: disposeBag)

        refreshControl.rx.controlEvent(.valueChanged).asDriver()
            .drive(onNext: output.viewShouldReloadProfile)
            .disposed(by: disposeBag)

        viewModel.isRefreshing
            .drive(activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel.isRefreshing
            .drive(collectionView.rx.isHidden)
            .disposed(by: disposeBag)
        viewModel.isRefreshing.map { $0 ? Constants.refreshControlHidden : Constants.refreshControlVisible }
            .drive(refreshControl.rx.alpha)
            .disposed(by: disposeBag)
    }

    func setupViewElements() {
        dock.setupViewElements()

        setupCollectioinView()
    }

    func setupConstraints() {
        dock.setupConstraints()

        collectionView.snp.removeConstraints()
        collectionView.snp.makeConstraints {
            $0.top.left.right.equalToSuperview()
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
    }

    func setupNavigationBar() {
        let newMessageButtonItem = UIBarButtonItem(title: Constants.newMessageTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(newMessageButtonPressed))
        navigationItem.rightBarButtonItem = newMessageButtonItem
    }

    func setupCollectioinView() {
        collectionView.collectionViewLayout = collectionViewLayout

        collectionView.register(TitledTextCollectionViewCell.self,
                                forCellWithReuseIdentifier: TitledTextCollectionViewCell.identifier)
        collectionView.register(TextCollectionViewHeader.self,
                                forSupplementaryViewOfKind: ProfileLayout.Element.sectionHeader.kind,
                                withReuseIdentifier: TextCollectionViewHeader.identifier)
        collectionView.register(ProfileCollectionViewHeader.self,
                                forSupplementaryViewOfKind: ProfileLayout.Element.header.kind,
                                withReuseIdentifier: ProfileCollectionViewHeader.identifier)
        collectionView.dataSource = dataSource
    }
}

private extension TeamMemberDetailsViewController {
    // MARK: - Actions

    @objc
    func newMessageButtonPressed() {
        output.viewDidTriggerNewMessageEvent()
    }
}
