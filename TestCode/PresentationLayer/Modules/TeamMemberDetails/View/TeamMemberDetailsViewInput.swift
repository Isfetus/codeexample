//
//  TeamMemberDetailsViewInput.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

/**
 The view input protocol.
 */
protocol TeamMemberDetailsViewInput: AnyObject {
    
    /// Setup initial state of the view.
    func setupInitialState()

    /// Method returns array of user info text view models.
    ///
    /// - Parameter model: team member view model.
    func setViewModel(_ model: ProfileViewModelProtocol)
}
