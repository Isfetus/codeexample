//
//  TeamMemberDetailsViewOutput.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

/**
 The view output protocol.
 */
protocol TeamMemberDetailsViewOutput: AnyObject {

    /// Method is called when the view is loaded.
    func viewDidTriggerReadyEvent()

    /// Method is called when user tap the new message button.
    func viewDidTriggerNewMessageEvent()

    /// Method is called when user pull refresh control.
    func viewShouldReloadProfile()
}
