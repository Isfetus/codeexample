//
//  TeamMemberDetailsAssembly.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

import Swinject
import SwinjectAutoregistration

final class TeamMemberDetailsAssembly {}

// MARK: - Assembly

extension TeamMemberDetailsAssembly: Assembly {
    // MARK: - Assembly

    func assemble(container: Container) {
        assembleView(container: container)
        assemblePresenter(container: container)
        assembleModule(container: container)
        assembleRoutes(container: container)
        assembleViewModelsBuilder(container: container)
        assembleUseCases(container: container)
    }
}

private extension TeamMemberDetailsAssembly {
    // MARK: - Private

    func assembleModule(container: Container) {
        container.autoregister(TeamMemberDetailsModule.self,
                               initializer: TeamMemberDetailsModule.init)
            .initCompleted { _, module in
                module.input = module.presenter
            }
    }

    func assembleView(container: Container) {
        container.autoregister(TeamMemberDetailsViewController.self,
                               initializer: TeamMemberDetailsViewController.init)
            .initCompleted { resolver, controller in
                controller.output = resolver ~> TeamMemberDetailsViewOutput.self
            }.implements(TeamMemberDetailsViewInput.self)
    }

    func assemblePresenter(container: Container) {
        container.autoregister(TeamMemberDetailsPresenter.self,
                               initializer: TeamMemberDetailsPresenter.init)
            .initCompleted { resolver, presenter in
                presenter.view = resolver ~> TeamMemberDetailsViewInput.self
                presenter.chatRoute.transition.sourceInput = presenter
            }
            .implements(TeamMemberDetailsViewOutput.self)
    }

    func assembleRoutes(container: Container) {
        container.autoregister(TeamMemberDetailsNewMessageRoute.self,
                               initializer: TeamMemberDetailsNewMessageRoute.init)
            .initCompleted { resolver, route in
                route.transition.sourceController = resolver ~> TeamMemberDetailsViewController.self
            }
    }

    func assembleViewModelsBuilder(container: Container) {
        container.autoregister(MemberProfileViewModelsBuilderProtocol.self,
                               initializer: MemberProfileViewModelsBuilder.init)
    }

    func assembleUseCases(container: Container) {
        container.autoregister(GetMemberProfileUseCase.self,
                               initializer: GetMemberProfileUseCase.init)
    }
}
