//
//  TeamMemberDetailsModuleInput.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

/**
 Module input protocol.
 */
protocol TeamMemberDetailsModuleInput: ErrorHandlerProtocol,
    ConversationsEventsProtocol,
    ActiveModuleEventsProtocol {

    /// Method for configuration the module.
    ///
    /// - Parameter teamMemberId: Team member identifier.
    func configure(teamMemberId: String?)
}
