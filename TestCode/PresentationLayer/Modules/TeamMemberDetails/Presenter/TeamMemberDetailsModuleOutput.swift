//
//  TeamMemberDetailsModuleOutput.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

/**
 Common output protocol of the module.
 */
typealias TeamMemberDetailsModuleOutput = ErrorHandlerProtocol & ConversationsEventsProtocol & ActiveModuleEventsProtocol
