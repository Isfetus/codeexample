//
//  TeamMemberDetailsPresenter.swift
//  TestCode
//
//  Created by Andrey Belkin on 14/03/2020.
//

import PromiseKit

final class TeamMemberDetailsPresenter {
    // Injection
    weak var view: TeamMemberDetailsViewInput!
    weak var moduleOutput: TeamMemberDetailsModuleOutput?
    // View models
    private let viewModelsBuilder: MemberProfileViewModelsBuilderProtocol
    private let viewModel = ProfileViewModel()
    private var teamMemberId: String?
    // Use cases
    private let memberProfileUseCase: GetMemberProfileUseCase
    // Routes
    let chatRoute: TeamMemberDetailsNewMessageRoute

    // MARK: - LifeCycle

    init(viewModelsBuilder: MemberProfileViewModelsBuilderProtocol,
         memberProfileUseCase: GetMemberProfileUseCase,
         chatRoute: TeamMemberDetailsNewMessageRoute) {
        self.viewModelsBuilder = viewModelsBuilder
        self.chatRoute = chatRoute
        self.memberProfileUseCase = memberProfileUseCase
    }
}

extension TeamMemberDetailsPresenter: TeamMemberDetailsViewOutput {
    // MARK: - TeamMemberDetailsViewOutput

    func viewDidTriggerReadyEvent() {
        view.setupInitialState()
        view.setViewModel(viewModel)
    }

    func viewDidTriggerNewMessageEvent() {
        guard let teamMemberId = self.teamMemberId else {
            fatalError()
        }

        chatRoute.go(teamMemberId: teamMemberId)
    }

    func viewShouldReloadProfile() {
        fetchMemberProfile {}
    }
}

extension TeamMemberDetailsPresenter: TeamMemberDetailsModuleInput {
    // MARK: - TeamMemberDetailsModuleInput

    func configure(teamMemberId: String?) {
        self.teamMemberId = teamMemberId

        startRefreshing()
        fetchMemberProfile(completion: stopRefreshing)
    }

    func handleError(_ error: ErrorDescription) {
        moduleOutput?.handleError(error)
    }

    func conversationsDidChange() {
        moduleOutput?.conversationsDidChange()
    }

    func moduleActivated(_ moduleInput: ActiveModuleProtocol?) {
        moduleOutput?.moduleActivated(moduleInput)
    }
}

private extension TeamMemberDetailsPresenter {
    // MARK: - Private

    func fetchMemberProfile(completion: @escaping () -> Void) {
        guard let teamMemberId = self.teamMemberId else {
            fatalError()
        }

        firstly {
            memberProfileUseCase.perform(teamMemberId)
        }.map {
            self.viewModelsBuilder.viewModel(for: $0)
        }.done {
            self.viewModel.personalInfoRelay.accept($0)
        }.ensure {
            completion()
        }.catch { error in
            self.viewModel.personalInfoRelay.accept([])
            let errorDescription = SystemError.from(error: error)
            self.moduleOutput?.handleError(errorDescription)
        }
    }

    func startRefreshing() {
        viewModel.isRefreshingRelay.accept(true)
    }

    func stopRefreshing() {
        viewModel.isRefreshingRelay.accept(false)
    }
}
